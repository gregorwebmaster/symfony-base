#!/bin/bash
export BASE_DIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

function dotenv() {
    cp -n ${BASE_DIR}/.envrc.dist ${BASE_DIR}/.envrc
    source .envrc
} && dotenv


function prepareDocker() {
    cp -n ${BASE_DIR}/docker/docker-compose.dist.yml ${BASE_DIR}/docker-compose.yml
}

function compose() {
    docker-compose ${1:-up -d} ${@:2}
}

function interactive() {
    docker-compose exec -it ${1:-php} ${2:-ash} ${@:3}
}

function runComposer() {
    docker run --rm -it -v ${BASE_DIR}/${PROJECT_NAME}:/workspace -w="/workspace" composer ${1:-install} ${@:2}
}

function installSymfony() {
    docker run --rm -it -v ${BASE_DIR}:/workspace -w="/workspace" composer create-project symfony/skeleton:"^${SYMFONY_VERSION}" ${PROJECT_NAME}
}

case $1 in
    c | config)
        prepareDocker
        ;;
    s | setup)
        prepareDocker
        compose
        runComposer
        ;;
    i | install)
        installSymfony
        runComposer composer config platform.php ${PHP_VERSION}
        runComposer update
        ;;
    ia | interactive)
        interactive 
        ;;
    l | logs)
        compose logs ${@}
        ;;
    cp | copmposer)
        runComposer ${@}
        ;;
esac